package br.com.blogfarm.shipping.client;

import spock.lang.Specification;

class BlogfarmShippingClientBuilderSpec extends Specification {

	def "builder should assemble a proper client with its attributes"() {
		when:
			def shippingClient = new BlogfarmShippingClientBuilder().make {
				host 'http://shipping.blogfarm.com.br'
				provider 'blogfarm'
				weight 0.25 as BigDecimal
				zipCode 2202002 as Long
			}
		
		then:
			shippingClient.host == 'http://shipping.blogfarm.com.br'
			shippingClient.provider == 'blogfarm'
			shippingClient.weight == (0.25 as BigDecimal)
			shippingClient.zipCode == (2202002 as Long)
	}
}
