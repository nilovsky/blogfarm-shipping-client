package br.com.blogfarm.shipping.client;

import groovy.json.JsonSlurper
import spock.lang.Specification

class ShippingQuoteVoMarshalling extends Specification {

	def "json should be marshalled to a ShippingQuoteVo object"() {
		given:
		def shippingQuoteJson = '{"eta":1,"price":4.42,"provider":"totalExpress","region":"RJ"}'
		ShippingQuoteVo vo = new ShippingQuoteVo(
								eta: 1, 
								price: 4.42 as BigDecimal, 
								provider: 'totalExpress',
								region: 'RJ'
							 )
		def convertedQuote = null
		
		when:
			def jsonSlurper = new JsonSlurper()
			convertedQuote = jsonSlurper.parseText(shippingQuoteJson) as ShippingQuoteVo
		
		then:
			convertedQuote.eta == vo.eta
			convertedQuote.price == vo.price
			convertedQuote.provider == vo.provider
			convertedQuote.region == vo.region
	}
}
