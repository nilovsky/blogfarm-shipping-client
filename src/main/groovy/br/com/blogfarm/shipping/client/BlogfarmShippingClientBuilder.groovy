package br.com.blogfarm.shipping.client

import groovy.lang.Closure;
import groovy.transform.CompileStatic;

class BlogfarmShippingClientBuilder {
	BlogfarmShippingClient client
	
	BlogfarmShippingClient make(Closure definition) {
		client = new BlogfarmShippingClient()
		runClosure definition
		client
	}

	def methodMissing(String name, arguments) {
		if (name in ['host', 'provider', 'weight', 'zipCode']) {
			client."$name" = arguments[0]
		}
	}

	private runClosure(Closure runClosure) {
		Closure runClone = runClosure.clone()
		runClone.delegate = this
		runClone.resolveStrategy = Closure.DELEGATE_ONLY
		runClone()
	}

	@CompileStatic
    static BlogfarmShippingClientBuilder newBuilder() {
        def builder = new BlogfarmShippingClientBuilder()
        builder.client = new BlogfarmShippingClient()
        return builder
    }

	@CompileStatic
    BlogfarmShippingClientBuilder host(String host) {
        this.client.host = host
        return this
    }

	@CompileStatic
    BlogfarmShippingClientBuilder provider(String provider) {
        this.client.provider = provider
        return this
    }

	@CompileStatic
    BlogfarmShippingClientBuilder zipCode(Long zipCode) {
        this.client.zipCode = zipCode
        return this
    }

	@CompileStatic
    BlogfarmShippingClientBuilder weight(BigDecimal weight) {
        this.client.weight = weight
        return this
    }

	@CompileStatic
    BlogfarmShippingClient build() {
        return client
    }
}
