package br.com.blogfarm.shipping.client

import groovy.json.JsonSlurper
import groovy.transform.Canonical
import groovy.transform.CompileStatic
import groovy.util.logging.Commons
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.RESTClient

@Canonical
@Commons
class BlogfarmShippingClient {
    def host
    def provider
    def weight
    def zipCode
        
    @CompileStatic
    ShippingQuoteVo getQuote() {
        return fetchData()
    }
        
    private ShippingQuoteVo fetchData() {
        def http = new RESTClient(host)
        def resp = http.get path : "/api/$provider/quotes/zipCode/$zipCode/weight/$weight"
        log.info resp.data
        return resp.data as ShippingQuoteVo
    }
}
