package br.com.blogfarm.shipping.client

import groovy.transform.Canonical;
import groovy.transform.CompileStatic;

@Canonical
@CompileStatic
class ShippingQuoteVo {
	Long eta
	BigDecimal price
	String provider
	String region
}
